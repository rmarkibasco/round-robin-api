const TAG = '[MirrorController]';

module.exports = {
    mirrorPost: function (req, res) {
        const ACTION = "[POST][mirror]"

        let min = Math.ceil(0);
        let max = Math.floor(5000);
        let timeout = Math.floor(Math.random() * (max - min + 1) + min);
          
        sails.log.info(TAG + ACTION + ' request body: ' + JSON.stringify(req.body) + ' timeout: ' + timeout);
        setTimeout(function(){
            res.json(200, req.body);
        }, timeout); 

    }

}  
