package com.example.roundrobin.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class AllNodesOfflineException extends RuntimeException {
    public AllNodesOfflineException(String message) {
        super(message);
    }

    public AllNodesOfflineException(String message, Throwable cause) {
        super(message, cause);
    }
}
