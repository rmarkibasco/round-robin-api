package com.example.roundrobin.service;

import com.example.roundrobin.domain.model.Endpoint;
import com.example.roundrobin.properties.RoutingProperties;
import com.example.roundrobin.service.exception.AllNodesOfflineException;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service(value = "round_robin")
public class RoundRobinImpl implements RoutingService {

    @Autowired
    private RoutingProperties routingProperties;

    @Autowired
    private RestTemplate restTemplate;
    private final AtomicInteger atomicIndex = new AtomicInteger(0);

    public RoundRobinImpl() {
    }

    public Map<String, Object> routeToUrl(Map<String, Object> request) {
        int initialIndex = atomicIndex.get();
        int indexToCall = atomicIndex.get();
        Endpoint endpoint = this.routingProperties.getEndpoints().get(initialIndex);
        if (!isSocketOpen(endpoint.getHost(), endpoint.getPort())) {
            indexToCall = getNextOpenEndpointIndex(indexToCall);
            endpoint = this.routingProperties.getEndpoints().get(indexToCall);
            incrementAtomicIndex(initialIndex, incrementEndpointsIndex(indexToCall));
        } else {
            incrementAtomicIndex(initialIndex, incrementEndpointsIndex(initialIndex));
        }

        return postRequest(endpoint.toString(), request);
    }

    /**
     * This method verifies if the socket of a given endpoint (hostname + port combination)
     * can receive new connections for an API call. Returns true if available, false otherwise.
     * @param hostname - endpoint hostname
     * @param port - port of the hostname
     * @return
     */
    protected boolean isSocketOpen(String hostname, int port) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(hostname, port), this.routingProperties.getTimeout());
            socket.close();
            return true;
        } catch (SocketTimeoutException e) {
            log.info("[isSocketOpen]: " + hostname + ":" + port + " - SOCKET TIMEOUT");
        } catch (IOException e) {
            log.info("[isSocketOpen]: " + hostname + ":" + port + " - UNABLE TO CONNECT");
        }
        return false;
    }

    /**
     * This method gets the next endpoint that is available for receiving
     * requests (the socket of that endpoint must be open) and this method
     * implements the iteration over the list of endpoints in routingProperties.
     *
     * The method will skip unavailable endpoints and move the index.
     *
     * @param index - index from the endpoint list that we want to use
     * @return int - new endpoints index that we want to send the request to
     */
    private int getNextOpenEndpointIndex(int index) {
        int endpointsSize = routingProperties.getEndpoints().size();
        int iterator = incrementEndpointsIndex(index);

        while(iterator <= endpointsSize) {
            Endpoint tempEndpoint = routingProperties.getEndpoints().get(iterator);
            if (iterator == index) {
                log.error("All application nodes are offline!");
                throw new AllNodesOfflineException("No Open Endpoint");
            }
            if (isSocketOpen(tempEndpoint.getHost(), tempEndpoint.getPort())) {
                break;
            }

            iterator = incrementEndpointsIndex(iterator);
        }
        return iterator;
    }

    private void incrementAtomicIndex(int initialIndex, int targetIndex) {
        this.atomicIndex.compareAndSet(initialIndex, targetIndex);
    }

    private int incrementEndpointsIndex(int index) {
        return index + 1 >= this.routingProperties.getEndpoints().size() ? 0 : index + 1;
    }

    /**
     *
     * @param url - url we want to POST the request to
     * @param requestBody - body we want to send with the request
     * @return - response of the POST request, null if status is not OK
     */
    public Map<String, Object> postRequest(String url, Map<String, Object> requestBody) {
        final String ACTION = "[postRequest]";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(requestBody, headers);

        log.info(ACTION + " Request to :" + url);
        Instant start = Instant.now();
        var response = this.restTemplate.postForEntity(url, entity, Map.class);
        long duration = Duration.between(start, Instant.now()).toMillis();
        log.info(ACTION + " Response   :" + url + " " + duration + "ms");

        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            return null;
        }
    }
}
