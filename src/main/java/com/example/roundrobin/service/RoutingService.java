package com.example.roundrobin.service;

import java.util.Map;

public interface RoutingService {

    /**
     * routeToUrl - load balances requests to diff URLs based on implementation
     * @param request - Map<String, Object> request received to be routed to another API
     * @return - Map<String, Object> response received from the routed API
     */
    Map<String, Object> routeToUrl(Map<String, Object> request);
}
