package com.example.roundrobin.domain.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Endpoint implements Serializable {

    private String host;
    private int port;
    private String path;

    @Override
    public String toString() {
        return "http://" + this.host + ":" + this.port + (this.path == null ? "" : this.path);
    }
}
