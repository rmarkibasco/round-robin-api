package com.example.roundrobin.controller;

import java.util.Map;

import com.example.roundrobin.service.RoutingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * This REST controller receives HTTP requests and routes
 * the calls to the listed endpoints in application.properties
 */
@RestController
@Slf4j
public class RoutingController {

	@Resource(name = "round_robin")
	private RoutingService routingService;

	@PostMapping("/route")
	public Map<String, Object> routeRequest(@RequestBody Map<String, Object> request){
		return routingService.routeToUrl(request);
	}
}
