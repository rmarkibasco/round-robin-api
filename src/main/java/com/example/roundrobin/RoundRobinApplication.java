package com.example.roundrobin;

import com.example.roundrobin.properties.RoutingProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@SpringBootApplication
public class RoundRobinApplication {

    @Autowired
    public RoutingProperties routingProperties;

    @Value("${request.timeout}")
    public int restTemplateTimeout;

    @Bean
    public RestTemplate restTemplate() {
        Duration timeoutDuration = Duration.ofMillis(restTemplateTimeout);
        return new RestTemplateBuilder()
                .setConnectTimeout(timeoutDuration)
                .setConnectTimeout(timeoutDuration)
                .build();
    }

    public static void main(String[] args) {
        SpringApplication.run(RoundRobinApplication.class, args);
    }

}
