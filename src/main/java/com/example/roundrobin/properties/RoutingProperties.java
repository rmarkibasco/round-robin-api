package com.example.roundrobin.properties;

import com.example.roundrobin.domain.model.Endpoint;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "routing")
public class RoutingProperties {
    private List<Endpoint> endpoints;
    private int timeout;
}
