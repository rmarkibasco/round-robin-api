Spring Boot Round Robin API (load balancer)
=
Spring Boot application that load balances endpoints in a Round Robin Algorithm

## Build
<i>From project root directory</i>
* Pure Maven: `mvn clean package`<br>

## Run
* Spring Boot Maven plugin (<i>root dir</i>): `mvn spring-boot:run`

### Routes:
`http://localhost:8080/route` - Round Robin Routing API<br>

## Application Properties
[application.properties]
* <b>server.port</b> - application default server port.<br>
* <b>routing.timeout</b> - configurable timeout for socket checking<br>

For the current version this file contains only a list of endpoints:<br>
`routing.endpoints[0].host=localhost`<br>
`routing.endpoints[0].port=1337`<br>
`routing.endpoints[0].path=/mirror`<br>

## Application Limitations and Improvements
For the current version, this API contains some limitations:<br>
* No strict locking in the index (iterator), we used AtomicInteger to enforce 
the iteration to be thread-safe but this does not completely avoid routing to the
same API several times when concurrency and traffic increases. We can improve this 
by possibly enforcing a cache that we can getAndUpdate strictly. 
* If a specific API in the list of endpoints is getting extremely slow responses, 
there is no way for us to reroute incoming traffic as it will still pass by that on this
basic Round Robin Implementation. We can improve this by adding a caching for the response times
of APIs and adding a logic that weighs the different APIs speed.

Simple Sails API (Mirror API)
=
## Build
<i>From project root directory (mirror-api)</i>
* `npm install`<br>

## Run
*  Basic run: `sails lift` 
*  For specific ports: `sails console --port <port>` 
